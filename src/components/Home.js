import React, {useEffect, useState} from "react"
import "./Home.css"
import {Container, Row, Col, Image} from "react-bootstrap";
import sun from "../assets/sun.svg";

import cloudQuestion from "../assets/cloudQuestion.svg";
import cloud1 from "../assets/cloud1.svg";
import cloud2 from "../assets/cloud2.svg";
import cloud3 from "../assets/cloud3.svg";

import team1 from "../assets/team1.svg";
import team2 from "../assets/team2.svg";
import team3 from "../assets/team3.svg";
import team4 from "../assets/team4.svg";

import tree41 from "../assets/tree4/1.png"
import tree42 from "../assets/tree4/2.png"
import tree43 from "../assets/tree4/3.png"
import tree44 from "../assets/tree4/4.png"
import tree45 from "../assets/tree4/5.png"
import tree46 from "../assets/tree4/6.png"
import tree47 from "../assets/tree4/7.png"
import tree48 from "../assets/tree4/8.png"
import tree49 from "../assets/tree4/9.png"
import tree410 from "../assets/tree4/10.png"
import tree411 from "../assets/tree4/11.png"
import tree412 from "../assets/tree4/12.png"
import tree413 from "../assets/tree4/13.png"
import tree414 from "../assets/tree4/14.png"
import tree415 from "../assets/tree4/15.png"

import tree31 from "../assets/tree3/1.png"
import tree32 from "../assets/tree3/2.png"
import tree33 from "../assets/tree3/3.png"
import tree34 from "../assets/tree3/4.png"
import tree35 from "../assets/tree3/5.png"
import tree36 from "../assets/tree3/6.png"
import tree37 from "../assets/tree3/7.png"
import tree38 from "../assets/tree3/8.png"
import tree39 from "../assets/tree3/9.png"
import tree310 from "../assets/tree3/10.png"
import tree311 from "../assets/tree3/11.png"
import tree312 from "../assets/tree3/12.png"
import tree313 from "../assets/tree3/13.png"
import tree314 from "../assets/tree3/14.png"
import tree315 from "../assets/tree3/15.png"

import tree21 from "../assets/tree2/1.png"
import tree22 from "../assets/tree2/2.png"
import tree23 from "../assets/tree2/3.png"
import tree24 from "../assets/tree2/4.png"
import tree25 from "../assets/tree2/5.png"
import tree26 from "../assets/tree2/6.png"
import tree27 from "../assets/tree2/7.png"
import tree28 from "../assets/tree2/8.png"
import tree29 from "../assets/tree2/9.png"
import tree210 from "../assets/tree2/10.png"
import tree211 from "../assets/tree2/11.png"
import tree212 from "../assets/tree2/12.png"
import tree213 from "../assets/tree2/13.png"
import tree214 from "../assets/tree2/14.png"
import tree215 from "../assets/tree2/15.png"

import tree11 from "../assets/tree1/1.png"
import tree12 from "../assets/tree1/2.png"
import tree13 from "../assets/tree1/3.png"
import tree14 from "../assets/tree1/4.png"
import tree15 from "../assets/tree1/5.png"
import tree16 from "../assets/tree1/6.png"
import tree17 from "../assets/tree1/7.png"
import tree18 from "../assets/tree1/8.png"
import tree19 from "../assets/tree1/9.png"
import tree110 from "../assets/tree1/10.png"
import tree111 from "../assets/tree1/11.png"
import tree112 from "../assets/tree1/12.png"
import tree113 from "../assets/tree1/13.png"
import tree114 from "../assets/tree1/14.png"
import tree115 from "../assets/tree1/15.png"

const Home = () => {
    const treeArr4 = [
        {
            photoTree4: tree41, photoTree3: tree31, photoTree2: tree21, photoTree1: tree11,
            style: {
                marginLeft: "9.5rem",
                position: "absolute",
                marginTop: "6.8rem",
                width: "45%"
            }
        },
        {
            photoTree4: tree42, photoTree3: tree32, photoTree2: tree22, photoTree1: tree12,
            style: {
                marginLeft: "9.5rem",
                position: "absolute",
                marginTop: "12.6rem",
            }
        },
        {
            photoTree4: tree46, photoTree3: tree36, photoTree2: tree26, photoTree1: tree16,
            style: {
                marginLeft: "9.5rem",
                position: "absolute",
                marginTop: "7.5rem"
            }
        },
        {
            photoTree4: tree47, photoTree3: tree37, photoTree2: tree27, photoTree1: tree17,
            style: {
                marginLeft: "9.1rem",
                position: "absolute",
                marginTop: "6.4rem"
            }
        },
        {
            photoTree4: tree49, photoTree3: tree39, photoTree2: tree29, photoTree1: tree19,
            style: {
                marginLeft: "5rem",
                position: "absolute",
                marginTop: "3.8rem"
            }
        },
        {
            photoTree4: tree410, photoTree3: tree310, photoTree2: tree210, photoTree1: tree110,
            style: {
                marginLeft: "4rem",
                position: "absolute",
                marginTop: "2.3rem"
            }
        },
        {
            photoTree4: tree412, photoTree3: tree312, photoTree2: tree212, photoTree1: tree112,
            style: {
                marginLeft: "1.5rem",
                position: "absolute",
                marginTop: "-1.8rem",
                width: "90%"
            }
        },
        {
            photoTree4: tree413, photoTree3: tree313, photoTree2: tree213, photoTree1: tree113,
            style: {
                marginLeft: "1.5rem",
                position: "absolute",
                marginTop: "-1.8rem",
                width: "90%"
            }
        }, {
            photoTree4: tree414, photoTree3: tree314, photoTree2: tree214, photoTree1: tree114,
            style: {
                marginLeft: "1.5rem",
                position: "absolute",
                marginTop: "-1.8rem",
                width: "90%"
            }
        },
        {
            photoTree4: tree415, photoTree3: tree315, photoTree2: tree215, photoTree1: tree115,
            style: {
                marginLeft: "1.5rem",
                position: "absolute",
                marginTop: "-1.4rem",
                width: "90%"
            }
        }
    ]
    const [check, setCheck] = useState(false)
    const [checkEl, setCheckEl] = useState(null)
    const [countQuestion, setCountQuestion] = useState(0)
    const [showAnswer, setShowAnswer] = useState(false)
    const [team1Count, setTeam1Count] = useState(0)
    const [team2Count, setTeam2Count] = useState(0)
    const [team3Count, setTeam3Count] = useState(0)
    const [team4Count, setTeam4Count] = useState(0)
    const TIMER = {
        PAUSED: "PAUSED",
        TICKING: "TICKING"
    };
    const [text, setText] = useState(60);
    const [current, setCurrent] = useState(null);
    const [timerState, setTimerState] = useState(TIMER.PAUSED);

    function isTicking() {
        return timerState === TIMER.TICKING;
    }

    function pause() {
        setTimerState(TIMER.PAUSED);
    }

    function handleStart() {
        if (isTicking()) {
            return;
        }

        if (!current) {
            setCurrent(Number(text));
        }
        setShowAnswer(true)

        setTimerState(TIMER.TICKING);
    }

    function handlePause() {
        pause();
    }

    useEffect(() => {
        let intervalId;

        function reset() {
            pause();
            setCurrent("0");
        }

        function pauseTimer() {
            clearInterval(intervalId);
        }

        function tick() {
            setCurrent((current) => {
                if (current === 0) {
                    reset();
                }

                return current - 1;
            });
        }

        switch (timerState) {
            case TIMER.PAUSED:
                break;
            case TIMER.TICKING:
                intervalId = setInterval(tick, 1000);
                break;
            default:
                break;
        }

        return pauseTimer;
    }, [timerState]);
    let textContent = [
        {
            question: "За какое минимальное время человек способен запомнить последовательность из 52 перетасованих карт и точно воспроизвести ее?",
            answer1: "12.7 cек",
            answer2: "10 мин",
            answer3: "1.5 мин",
            true: "answer2"
        },
        {
            question: "Привіт молодий чоловіче?2",
            answer1: "Хай2",
            answer2: "хАЮ2",
            answer3: "Хелоу2",
            true: "answer3"
        },
        {
            question: "Привіт молодий чоловіче?3",
            answer1: "Хай3",
            answer2: "хАЮ3",
        },
        {
            question: "Привіт молодий чоловіче?4",
            answer1: "Хай4",
            answer2: "хАЮ4",
            answer3: "Хелоу4"
        },
        {
            question: "Дякую за увагу"
        }
    ]
    let countQuestionHandler = () => {
        setCountQuestion(countQuestion < textContent.length - 1 ? countQuestion + 1 : countQuestion)
        setCurrent(60)
        setCheckEl(null)
        setCheck(false)
        setShowAnswer(false)
        handlePause()
    }
    let checkAnswerHandler = (cloud) => {
        setCheck(!check)
        setCheckEl(cloud)
        handlePause()
    }
    console.log(`Первая команда очков ${team1Count}`)
    console.log(`Вторая команда очков ${team2Count}`)
    console.log(`Третья команда очков ${team3Count}`)
    console.log(`Четвертая команда очков ${team4Count}`)

    return (
        <div className="app">
            <Container fluid>
                <Row className="ml-5">
                    <Col xs={2}>
                        <Image src={sun} className="w-100 h-75"/>
                        <h1 className={current > 9 || current === null ? "numberSun" : "numberSun9"}
                            onKeyDown={e => console.log(e)}>{current === null ? '60' : current}</h1>
                        <button onClick={handleStart} disabled={isTicking()} className="start">
                            Start
                        </button>
                        <button onClick={handlePause} disabled={!isTicking()} className="pause">
                            Pause
                        </button>
                        <button className="reset" onClick={() => setCurrent(null)}>RESET</button>
                    </Col>
                    {textContent[countQuestion].question ?
                        (<Col>
                            <Image src={cloudQuestion} className="cloudQuestionSize"/>
                            <h1 className="cloudQuestion"
                                onClick={() => countQuestionHandler()}>{textContent[countQuestion].question || ""}</h1>
                        </Col>) : (<Col>
                            <Image src={cloudQuestion} className="w-100 h-100"/>
                            <h1 className="cloudQuestion" onClick={() => countQuestionHandler()}>Дякую за увагу)</h1>
                        </Col>)
                    }

                </Row>
                <Row className="justify-content-md-center answerRow">
                    {showAnswer && textContent[countQuestion].answer1 ?
                        <Col xs={3} onClick={() => checkAnswerHandler('cloud1')}>
                            <object type="image/svg+xml" data={cloud1}
                                    className={checkEl !== 'cloud1'
                                        ? "cloudAnswer" : textContent[countQuestion].true === "answer1"
                                            ? "cloudAnswerCheck" : "cloudAnswerMistake"}
                                    loading="lazy"/>
                            <h2 className="answer">{textContent[countQuestion].answer1}</h2>
                        </Col> : ""}
                    {showAnswer && textContent[countQuestion].answer2 ?
                        <Col xs={3} onClick={() => checkAnswerHandler('cloud2')}>
                            <object type="image/svg+xml" data={cloud2}
                                    className={checkEl !== 'cloud2'
                                        ? "cloudAnswer" : textContent[countQuestion].true === "answer2"
                                            ? "cloudAnswerCheck" : "cloudAnswerMistake"} loading="lazy"/>
                            <h2 className="answer">{textContent[countQuestion].answer2}</h2>
                        </Col> : ""}
                    {showAnswer && textContent[countQuestion].answer3 ?
                        <Col xs={3} onClick={() => checkAnswerHandler('cloud3')}>
                            <object type="image/svg+xml" data={cloud3}
                                    className={checkEl !== 'cloud3'
                                        ? "cloudAnswer" : textContent[countQuestion].true === "answer3"
                                            ? "cloudAnswerCheck" : "cloudAnswerMistake"} loading="lazy"/>
                            <h2 className="answer">{textContent[countQuestion].answer3}</h2>
                        </Col> : ""}
                </Row>
                <Row className="d-flex justify-content-center">
                    <Col xs={3}>
                        <Image src={treeArr4[team1Count].photoTree1}
                               style={treeArr4[team1Count].style}
                               onClick={() => setTeam1Count(team1Count > 0 ? team1Count - 1 : team1Count)}/>
                        <Image src={team1} className="team"
                               onClick={() => setTeam1Count(team1Count < 9 ? team1Count + 1 : team1Count)}/>
                    </Col>
                    <Col xs={3}>
                        <Image src={treeArr4[team2Count].photoTree2}
                               style={treeArr4[team2Count].style}
                               onClick={() => setTeam2Count(team2Count > 0 ? team2Count - 1 : team2Count)}/>
                        <Image src={team2} className="team"
                               onClick={() => setTeam2Count(team2Count < 9 ? team2Count + 1 : team2Count)}/>
                    </Col>
                    <Col xs={3}>
                        <Image src={treeArr4[team3Count].photoTree3}
                               style={treeArr4[team3Count].style}
                               onClick={() => setTeam3Count(team3Count > 0 ? team3Count - 1 : team3Count)}/>
                        <Image src={team3} className="team"
                               onClick={() => setTeam3Count(team3Count < 9 ? team3Count + 1 : team3Count)}/>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Home
